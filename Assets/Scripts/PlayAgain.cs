﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PlayAgain: MonoBehaviour {


    public  Text numberRequired;

    public double numRequired;

    public bool playAgain;

    PlayerControl playerControl;

    public  Text yes;
    public  Text no;

	

    void Start()
    {
        playerControl = FindObjectOfType<PlayerControl>();
    }
	public void YesClick()
    {
        if (PlayerPrefs.GetInt("Bananas") >= numRequired)
        {
            Destroy(gameObject);

            Time.timeScale = 1f;
            playerControl.playAgainInstantiated = false;
        }
        else
        {
            return;
        }
        PlayerPrefs.SetInt("Bananas", PlayerPrefs.GetInt("Bananas") - Convert.ToInt32(numRequired));
        PlayerPrefs.Save();
    }

    public void NoClick()
    {
        playerControl.dead = true;
        Destroy(playerControl.gameObject);
        GameStateManager.EndGame();
    }

    void Update()
    {

        if(PlayerPrefs.GetInt("Bananas")<numRequired)
        {
            yes.color = Color.clear;
        }
        
        numRequired = Math.Pow(Convert.ToDouble(2) ,Convert.ToDouble(playerControl.numberOfTimesInstantiated));
        numberRequired.text = "" + numRequired;

        
    }
}
