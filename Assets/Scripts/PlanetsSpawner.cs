﻿using UnityEngine;
using System.Collections;

public class PlanetsSpawner : MonoBehaviour {



	public Transform[] prefab;
	
	public Transform[] spawnPoints;
	
	//Score score;
	
	
	
	[SerializeField]
	
	float spawnTime=1f;
	
	
	
	private float curSpawnTime;

    PlayerControl playerControl;
	
	
	void Start()
		
	{
		
		curSpawnTime = spawnTime;

        //score = FindObjectOfType<Score>();

        playerControl= FindObjectOfType<PlayerControl>();
		
	}
	
	void Update()
		
	{
		
		curSpawnTime-=1 * Time.deltaTime;
		
		if(curSpawnTime <= 0)			
		{			
			Spawn();
			
			curSpawnTime = spawnTime;			
		}

        if(playerControl.yMove<=5)
        {
            spawnTime = 1f;
        }

        if(playerControl.yMove>5 && playerControl.yMove<=7.5)
        {
            spawnTime = 0.75f;
        }
        if (playerControl.yMove > 7.5 && playerControl.yMove <= 10)
        {
            spawnTime = 0.6f;
        }
        if(playerControl.yMove>10 && playerControl.yMove<=20)
        {
            spawnTime = 0.25f;
        }
    }
	
	void Spawn()
	{
		Instantiate(prefab[Random.Range(0, 1)], spawnPoints[Random.Range(0,3)].transform.position, Quaternion.identity);
		
		
	}

}
