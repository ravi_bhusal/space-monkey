﻿using UnityEngine;
using System.Collections;

public class Banana : MonoBehaviour {

    GameObject checkPosition;

    void Start ()
    {
        checkPosition = GameObject.Find("CheckPosition");
    }
	
	
	void Update ()
    {
        if (transform.position.y < checkPosition.transform.position.y)
        {
            Destroy(gameObject);

        }
    }

   
}
