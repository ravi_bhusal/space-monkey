﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Rigidbody2D))]
public class PlayerControl : MonoBehaviour {


	Rigidbody2D rb;
	//float move=0f;

	public bool dead;

    public float yMove;

    public bool boosted;

    public float boostedTime = 5f;

    public bool boostEndingAnimation;

    public bool playAgainInstantiated;

    Score score;

   public Animator anim;

    public Transform playAgainPrefab;
    
    public int numberOfTimesInstantiated;

    public Transform playAgainSpawner;
    
	void Start()
	{
		rb=GetComponent<Rigidbody2D>();
        score = FindObjectOfType<Score>();
       
        numberOfTimesInstantiated = 0;
        Time.timeScale = 1f;
       


	}

	void FixedUpdate()
	{
        anim.SetBool("boostEndingAnimation", boostEndingAnimation);
        if(boosted)
        {
            boostedTime -= 1 * Time.deltaTime;
            rb.mass = 1000;
            
        }
        else
        {
            rb.mass = 1;
        }
        if(boostedTime<=0)
        {
            boosted = false;
            
        }
        if(boostedTime<2 && boostedTime>0)
        {
            boostEndingAnimation = true;
            yMove = 10f;
            //anim.SetFloat("boostedTime", boostedTime);
        }
        else if(boostedTime>2 || boosted==false)
        {
            boostEndingAnimation = false;
        }




          if (score.score < 5000 && !boosted)
          {
              yMove = 5f;
          }
          else if(score.score>5000 && score.score<18000 && !boosted)
          {
              yMove = 7.5f;
          }
          else if(score.score>18000 && !boosted)
          {
              yMove = 9f;
          }
          else if(boosted && boostedTime > 2)
          {
              yMove = 20f;
          }
          float move= Input.acceleration.x;
          rb.velocity=new Vector2(move*20,yMove);
          
       // float move = Input.GetAxis("Horizontal");
       // rb.velocity = new Vector2(move*5, yMove);


        Vector3 minScreenBounds = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        Vector3 maxScreenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minScreenBounds.x+0.55f, maxScreenBounds.x-0.55f), Mathf.Clamp(transform.position.y, minScreenBounds.y, maxScreenBounds.y), transform.position.z);

    }

    void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.tag=="Killer")
		{
            //WaitForSeconds(5);
            if (!boosted)
            {
               
                
                    Instantiate(playAgainPrefab, playAgainSpawner.transform.position, Quaternion.identity);
                    numberOfTimesInstantiated++;
                    Time.timeScale = 0.0f;
                    Destroy(col.gameObject, 0.01f);
                playAgainInstantiated = true;
                
                   
                
                /*if(playAgain.playAgain && PlayerPrefs.GetInt("Bananas")>playAgain.numRequired)
                {
                    Destroy(playAgainPrefab,0.01f);
                    Destroy(col.gameObject, 0.01f);
                    Time.timeScale = 1f;
                }
                else if(!playAgain.playAgain)
                {
                    dead = true;
                    Destroy(gameObject);
                    GameStateManager.EndGame();
                }*/
              
            }

            if(boosted)
            {
                Destroy(col.gameObject);
            }
			
		}
        
        
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Booster")
        {
            boosted = true;
            boostedTime = 7f;
            Destroy(col.gameObject);
        }

        if (col.gameObject.tag == "Banana")
        {
            PlayerPrefs.SetInt("Bananas", PlayerPrefs.GetInt("Bananas", 5) + 1);
            PlayerPrefs.Save();
            Destroy(col.gameObject);
        }
    }

}
