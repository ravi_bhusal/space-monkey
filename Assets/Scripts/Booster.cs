﻿using UnityEngine;
using System.Collections;

public class Booster : MonoBehaviour {


    
    PlayerControl playerControl;
    GameObject checkPosition;
    void Start ()
    {
        checkPosition = GameObject.Find("CheckPosition");
        
        playerControl = FindObjectOfType<PlayerControl>();
	}
	
   

    void Update()
    {
        if (transform.position.y < checkPosition.transform.position.y)
        {
            Destroy(gameObject);

        }
    }
}
