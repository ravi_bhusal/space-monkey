﻿using UnityEngine;
using System.Collections;

public class BoosterSpawner : MonoBehaviour {
    public Transform prefab;

    public Transform[] spawnPoints;




    [SerializeField]

    



    public float curSpawnTime;


    void Start()

    {
        
        curSpawnTime = Random.Range(15,20);

        //score = FindObjectOfType<Score>();

    }

    void Update()

    {

        curSpawnTime -= 1 * Time.deltaTime;

        if (curSpawnTime <= 0)

        {

            Spawn();

            curSpawnTime = Random.Range(20, 25);


        }
    }

    void Spawn()
    {
        Vector3 point = spawnPoints[Random.Range(0, 3)].transform.position;
        Collider2D hitColliders = Physics2D.OverlapCircle(point, 3f);
        if (hitColliders == null)
        {
            Instantiate(prefab, point, Quaternion.Euler(0,0,90));

        }


    }

}
