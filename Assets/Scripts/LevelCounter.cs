﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
public class LevelCounter : MonoBehaviour {

     public int i=0;

    InterstitialAd interstitial;
    BannerView bannerView;

    void Awake()
    {
        DontDestroyOnLoad(this);
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
       
    }

    
    
    void OnLevelWasLoaded(int level)
    {

        if (level == 0)
        {

            interstitial.Destroy();
            PlayerPrefs.SetInt("GameOver", PlayerPrefs.GetInt("GameOver") + 1);
            if (PlayerPrefs.GetInt("LevelLoaded") > 1)
            {
                bannerView.Show();
                PlayerPrefs.SetInt("GameOver", 0);
            }
            


        }
        if(level==2)
        {

            RequestBanner();

            PlayerPrefs.SetInt("LevelLoaded", PlayerPrefs.GetInt("LevelLoaded") + 1);

            if (PlayerPrefs.GetInt("LevelLoaded") > 3)
            {
                if (interstitial.IsLoaded())
                {
                    interstitial.Show();
                    Debug.Log("Interstitial shown");
                }

                PlayerPrefs.SetInt("LevelLoaded", 0);
            }
        }

        if(level==1)
        {
            bannerView.Hide();
            bannerView.Destroy();
            RequestInterstitial();


        }
       
    }
  
    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-6628476524899710/9951763188";
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }

    private void RequestBanner()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-6628476524899710/8475029985";
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);

    }

}
