﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GoogleMobileAds.Api;

public class GameOver : MonoBehaviour {

	// Use this for initialization
	ScorePassers scorePasser;

	public int highScore;
	public int yourScore;
	public int bananasCollected;

	bool called;

    public Text scoreText;
    public Text highScoreText;

   


    void Start()
    {
        scorePasser = FindObjectOfType<ScorePassers>();

        bananasCollected = PlayerPrefs.GetInt("Bananas");
     
    }
	


	void Update ()
	{
        yourScore = scorePasser.yourFinalScore;

        scoreText.text = "Score: " + yourScore;

        if (yourScore > PlayerPrefs.GetInt("High Score"))
        {

            PlayerPrefs.SetInt("High Score", yourScore);
            PlayerPrefs.Save();
        }

        highScoreText.text = "Best Score: " + PlayerPrefs.GetInt("High Score");


    }

	public void PlayAgain()
	{
		Application.LoadLevel(0);
	}


   

    
}
