﻿using UnityEngine;
using System.Collections;

public class Asteroids : MonoBehaviour {

	[SerializeField]
	private bool isMoving;
	Rigidbody2D rb;

	PlayerControl playerControl;

	GameObject checkPosition;

   
	void Start()
	{
		checkPosition=GameObject.Find("CheckPosition");
		playerControl=FindObjectOfType<PlayerControl>();
		rb=GetComponent<Rigidbody2D>();
	}
	

	void Update()
	{
	  if(isMoving)
		{
			rb.gravityScale=1.5f;
		}
	  else
		{
			rb.gravityScale=0;
		}

		transform.Rotate(0,0,50*Time.deltaTime);

		Vector2 difference=playerControl.transform.position-transform.position;
      
		rb.velocity=new Vector2(difference.x,difference.y);


		if(transform.position.y<checkPosition.transform.position.y)
		{
			Destroy(gameObject);

		}
	
	}
}
