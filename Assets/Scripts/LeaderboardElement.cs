﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class LeaderboardElement : MonoBehaviour {

    public Image Background;
    public RawImage ProfilePicture;
    public Text Rank;
    public Text Name;
    public Text Score;

    public void SetupElement(int rank, object entryObj)
    {
        
        var entry = (Dictionary<string, object>)entryObj;
        var user = (Dictionary<string, object>)entry["user"];

        Rank.text = rank.ToString() + ".";

        Name.text = ((string)user["name"]).Split(new char[] { ' ' })[0];
      
        Score.text = "" + GraphUtil.GetScoreFromEntry(entry).ToString();
       

        Texture picture;
        if (GameStateManager.FriendImages.TryGetValue((string)user["id"], out picture))
        {
            ProfilePicture.texture = picture;
        }
    }
}
