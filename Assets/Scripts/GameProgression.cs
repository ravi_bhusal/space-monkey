﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class GameProgression : MonoBehaviour {



	GameObject planetsSpawner;
	GameObject asteroidSpawner;
	GameObject alienSpawner;


	


	String[] array= new String[7];

    String lastArray;
    String nextArray;

	float changeTime=20f;
	float curTime;
	void Awake () 
	{
	   planetsSpawner=GameObject.Find("PlanetsSpawner");
	   asteroidSpawner=GameObject.Find("AsteroidSpawner");
		alienSpawner=GameObject.Find("AlienSpawner");
	
		array[0]="OnlyAliens";
		array[1]="OnlyAsteroids";
		array[2]="OnlyPlanets";
	

	}

	void Start()
	{
		curTime=changeTime;
		planetsSpawner.SetActive(true);
		asteroidSpawner.SetActive(false);
		alienSpawner.SetActive(false);
	}

	
	// Update is called once per frame
	void Update ()
	{

		curTime-=1*Time.deltaTime;


		if(curTime<0)
		{
            do
            {
                nextArray = array[UnityEngine.Random.Range(0, 3)];
            } while (nextArray == lastArray);
		 Invoke(nextArray,1);
         lastArray = nextArray;
		 curTime=changeTime;
		}




	
	}

	void OnlyAsteroids()
	{
		planetsSpawner.SetActive(false);
		asteroidSpawner.SetActive(true);
		alienSpawner.SetActive(false);
	}

	void OnlyPlanets()
	{
		planetsSpawner.SetActive(true);
		asteroidSpawner.SetActive(false);
		alienSpawner.SetActive(false);
	}

	void OnlyAliens()
	{
		planetsSpawner.SetActive(false);
		asteroidSpawner.SetActive(false);
		alienSpawner.SetActive(true);
	}

	







}
