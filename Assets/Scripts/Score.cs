﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class Score : MonoBehaviour {



	public  int score=0;
	
	public int finalScore; 

	PlayerControl playerControl;

	public Text scoreText;


	private static Score instanceRef;
	ScorePassers scorePasser;

    public Text pauseText;
	

	void Start () 
	{
		playerControl=FindObjectOfType<PlayerControl>();
		scorePasser=FindObjectOfType<ScorePassers>();
        pauseText.text = "Pause";
       
	}
	void FixedUpdate()
	{
		if(playerControl.dead==false && Time.timeScale!=0.0f)
		{
            score += 1 * Convert.ToInt32(Math.Ceiling(playerControl.yMove));
		SetScore();
		}

		//finalScore=score+starsCollected*10;
		scorePasser.GetYourScore(score);
		


	}


	void SetScore()
	{
		scoreText.text=""+score;
	}

    public void Pause()
    {
        if (pauseText.text == "Pause")
        {
            Time.timeScale = 0f;
            pauseText.text = "Play";
        }
        else if (pauseText.text == "Play")
        {
            Time.timeScale = 1f;
            pauseText.text = "Pause";
        }

    }

    void Update()
    {
        if(playerControl.playAgainInstantiated)
        {
            pauseText.enabled = false;
        }
        else if(!playerControl.playAgainInstantiated)
        {
            pauseText.enabled = true;
        }
    }
}
