﻿using UnityEngine;
using System;
using UnityEngine.Advertisements;




public class GetFreeBananas : MonoBehaviour {


    public bool isCompleted;
    ShowOptions options;
    StartMenu startMenu;
    // Use this for initialization
    void Start () {


        options = new ShowOptions();
        options.resultCallback = HandleShowResult;
       
       

    }

    void OnMouseDown()
    {

       
        if (Advertisement.IsReady())
        {
            Advertisement.Show(null, options);
            Destroy(gameObject, 0.01f);

        }
       

        
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                PlayerPrefs.SetInt("Bananas", PlayerPrefs.GetInt("Bananas") + 5);               
                PlayerPrefs.Save();
               
         
                break;
            case ShowResult.Skipped:
                Debug.LogWarning("Video was skipped.");
                break;
            case ShowResult.Failed:
                Debug.LogError("Video failed to show.");
                break;
        }
    }

    void Update()
    {
        Destroy(gameObject, 15f);
    }

    void OnDestroy()
    {
        PlayerPrefs.SetInt("Ad completed", 1);
        PlayerPrefs.Save();
    }

}
