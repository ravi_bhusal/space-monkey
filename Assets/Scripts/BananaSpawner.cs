﻿using UnityEngine;
using System.Collections;

public class BananaSpawner : MonoBehaviour {

    public Transform prefab;

    public Transform[] spawnPoints;




    [SerializeField]
    public float curSpawnTime;


    void Start()

    {

        curSpawnTime = Random.Range(45, 60);

        //score = FindObjectOfType<Score>();

    }

    void Update()

    {

        curSpawnTime -= 1 * Time.deltaTime;

        if (curSpawnTime <= 0)

        {

            Spawn();

            curSpawnTime = Random.Range(45, 60);


        }
    }

    void Spawn()
    {
        Vector3 point = spawnPoints[Random.Range(0, 3)].transform.position;
        Collider2D hitColliders = Physics2D.OverlapCircle(point, 3f);
        if (hitColliders == null)
        {
            Instantiate(prefab, point, Quaternion.identity);

        }


    }

}
