﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class Tiling : MonoBehaviour {

    public int offsetX = 2; //for no weird errors between the camera and the object

    //these are needed to know if we need to instantiate stuff
    public bool hasATopBuddy = false;
    public bool hasABottomBuddy = false;

    public bool reverseScales = false; //used if the object is not tilable

    private float spriteHeight = 0f; //the width of sprite
    private Camera cam;
    private Transform myTransform;


    void Awake()
    {
        cam = Camera.main;
        myTransform = transform;
    }
    // Use this for initialization
    void Start() {
        SpriteRenderer sRenderer = GetComponent<SpriteRenderer>();
        spriteHeight = sRenderer.sprite.bounds.size.y;
    }

    // Update is called once per frame
    void Update() {
        Destroy(gameObject, 10f);
        //does it still need buddies? if not then do nothing
        if (hasATopBuddy == false || hasABottomBuddy == false)
        {
            //calculate the camera's extend(half the width) of what the camera can see in world coordinates)
			float cameraVerticalExtend = (cam.orthographicSize * Screen.height / Screen.width);

            //calculate the x position where the camera can see the edge of the sprite
			float edgeVisiblePositionBottom = (myTransform.position.y + spriteHeight / 2) - cameraVerticalExtend;
			float edgeVisiblePositionTop = (myTransform.position.y - spriteHeight / 2) + cameraVerticalExtend;
            //checking the edge of our element an calling MakeNewBuddy if we can
			if (cam.transform.position.y >= edgeVisiblePositionBottom - offsetX && hasABottomBuddy == false)
            {
                MakeNewBuddy(1);
				hasABottomBuddy = true;
            }
			else if (cam.transform.position.y <= edgeVisiblePositionTop + offsetX && hasATopBuddy == false)
            {
                MakeNewBuddy(-1);
                hasATopBuddy = true;
            }
        }
    }
    //a function that creates a new buddy if required
    void MakeNewBuddy(int rightOrLeft)
    {
        //calculating the new position of our pody
		Vector3 newPosition = new Vector3 (myTransform.position.x ,myTransform.position.y+ spriteHeight * rightOrLeft, myTransform.position.z);
       Transform newBuddy= Instantiate(myTransform, newPosition, myTransform.rotation) as Transform;

       
        //if not tilable let's reverse the x size of  our object to get rid of ugly seams
        if (reverseScales == true)
        {
            newBuddy.localScale = new Vector3(newBuddy.localScale.x * -1, newBuddy.localScale.y, newBuddy.localScale.z);
        }
        newBuddy.parent = myTransform.parent;
        if(rightOrLeft>0)
        {
            newBuddy.GetComponent<Tiling>().hasATopBuddy = true;
        }
        else
        {
            newBuddy.GetComponent<Tiling>().hasABottomBuddy = true;
        }
        

    }
}
