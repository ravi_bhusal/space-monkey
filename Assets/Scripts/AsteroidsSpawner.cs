﻿using UnityEngine;
using System.Collections;

public class AliensSpawner : MonoBehaviour {

	public Transform[] prefab;
	
	public Transform[] spawnPoints;
				
	//Score score;
	
	
	
	[SerializeField]
	
	float spawnTime=1f;
	
	
	
	private float curSpawnTime;
	
	
	void Start()
		
	{
		
		curSpawnTime = spawnTime;
		
		//score = FindObjectOfType<Score>();
		
	}
	
	void Update()
		
	{
		
		curSpawnTime-=1 * Time.deltaTime;
		
		if(curSpawnTime <= 0)
			
		{
			
			Spawn();
			
			curSpawnTime = spawnTime;
			
			
		}
	}

	void Spawn()
	{
		Instantiate(prefab[Random.Range(0, 2)], spawnPoints[Random.Range(0,6)].transform.position, Quaternion.identity);
		

	}



}
