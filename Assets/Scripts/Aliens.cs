﻿using UnityEngine;
using System.Collections;

public class Aliens : MonoBehaviour {


	Rigidbody2D rb;

	PlayerControl playerControl;
	GameObject checkPosition;

    int chaseSpeed;
	void Start ()
	{
		checkPosition=GameObject.Find("CheckPosition");
		playerControl=FindObjectOfType<PlayerControl>();
		rb=GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector2 difference=playerControl.transform.position-transform.position;
		difference.Normalize();

		rb.velocity=new Vector2(difference.x*chaseSpeed, difference.y*chaseSpeed);

        if(playerControl.yMove<9)
        {
            chaseSpeed = 3;
        }
        else if(playerControl.yMove>=9)
        {
            chaseSpeed = 4;
        }
		if(transform.position.y<checkPosition.transform.position.y)
		{
			Destroy(gameObject);
			
		}
	}
}
