﻿using UnityEngine;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine.UI;
using System;
using GoogleMobileAds.Api;
using System.Linq;


public class StartMenu : MonoBehaviour {

	public Canvas startMenu;
	public Canvas aboutMenu;
	public Canvas settingsMenu;
	public Canvas helpMenu;
    public Canvas LeaderBoardMenu;

    public GameObject LeaderboardPanel;
    public GameObject LeaderboardItemPrefab;
    public ScrollRect LeaderboardScrollRect;


    public Text numberOfBananasText;

   

    public bool firstExecution;

    public string lastAd;

    public int numberOfBananas;

    public int adCompleted;
    

    public  Text musicText;

    public GameObject getBananasPrefab;
    public Transform getBananasPrefabPosition;



    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback,OnHideUnity);
        }
        else {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }

        
    }


    void Start()
    {
        startMenu = startMenu.GetComponent<Canvas>();
        aboutMenu = aboutMenu.GetComponent<Canvas>();
        settingsMenu = settingsMenu.GetComponent<Canvas>();
        helpMenu = helpMenu.GetComponent<Canvas>();
        LeaderBoardMenu = LeaderBoardMenu.GetComponent<Canvas>();

        startMenu.enabled = true;
        aboutMenu.enabled = false;
        settingsMenu.enabled = false;
        helpMenu.enabled = false;
        LeaderBoardMenu.enabled = false;

       

        if (FB.IsLoggedIn)
        {
            FBGraph.GetPlayerInfo();
            FBGraph.GetFriends();
            FBGraph.GetScores();

        }

        PlayerPrefs.SetString("Last ad", PlayerPrefs.GetString("Last ad"));
        PlayerPrefs.SetString("music", PlayerPrefs.GetString("music","Music: On"));
        musicText.text = PlayerPrefs.GetString("music");

        PlayerPrefs.SetInt("Bananas", PlayerPrefs.GetInt("Bananas",5));

       


    }
	



	public void Play()
	{
		Application.LoadLevel(1);
	}

	public void About()
	{
		startMenu.enabled=false;
		settingsMenu.enabled=false;
		aboutMenu.enabled=true;
        LeaderBoardMenu.enabled = false;
        helpMenu.enabled = false;
    }

	public void Settings()
	{
		startMenu.enabled=false;
		settingsMenu.enabled=true;
		aboutMenu.enabled=false;
        LeaderBoardMenu.enabled = false;
        helpMenu.enabled = false;
    }

	public void LeaderBoards()
	{
        startMenu.enabled = false;
        settingsMenu.enabled = false;
        aboutMenu.enabled = false;
        helpMenu.enabled = false;
        LeaderBoardMenu.enabled = true;

      
        ShowLeaderboard();
       
       

       

    }

	

	public void Help()
	{
		startMenu.enabled=false;
		settingsMenu.enabled=false;
		aboutMenu.enabled=false;
        LeaderBoardMenu.enabled = false;
        helpMenu.enabled=true;
	}

	public void Back()
	{
		startMenu.enabled=true;
		aboutMenu.enabled=false;
		settingsMenu.enabled=false;
        LeaderBoardMenu.enabled = false;
        helpMenu.enabled=false;
	}

    public void FacebookConnect()
    {
        var perms = new List<string>() { "public_profile", "email", "user_friends" };    
        FB.LogInWithReadPermissions(perms, AuthCallback);
        
    }

    private void InitCallback()
    {
        Debug.Log("Fb init done");
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            FBGraph.GetPlayerInfo();
            FBGraph.GetFriends();
            FBGraph.GetInvitableFriends();
            FBGraph.GetScores();

            var perms = new List<string>() { "publish_actions" };
            FB.LogInWithPublishPermissions(perms);
           
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
        }
        else {
            Debug.Log("User cancelled login");
        }
    }

    

    

    void ShowLeaderboard()
    {
        if (FB.IsLoggedIn)
        {
            FBGraph.GetPlayerInfo();
            FBGraph.GetFriends();
            FBGraph.GetScores();
        }
        var scores = GameStateManager.Scores;
        if (GameStateManager.ScoresReady && scores.Count > 0)
        {
            // Clear out previous leaderboard
            Transform[] childLBElements = LeaderboardPanel.GetComponentsInChildren<Transform>();
            foreach (Transform childObject in childLBElements)
            {
                if (!LeaderboardPanel.transform.IsChildOf(childObject.transform))
                {
                    Destroy(childObject.gameObject);
                }
            }

            // Populate leaderboard
            for (int i = 0; i < scores.Count; i++)
            {

                GameObject LBgameObject = Instantiate(LeaderboardItemPrefab) as GameObject;
                LeaderboardElement LBelement = LBgameObject.GetComponent<LeaderboardElement>();
                LBelement.SetupElement(i + 1, scores[i]);
                LBelement.transform.SetParent(LeaderboardPanel.transform, false);
            }

            // Scroll to top
            LeaderboardScrollRect.verticalNormalizedPosition = 1f;
        }
    }

    void Update()
    {
        numberOfBananasText.text = "" + PlayerPrefs.GetInt("Bananas");
        PlayerPrefs.SetInt("First Execution", PlayerPrefs.GetInt("First Execution", 1));
        PlayerPrefs.Save();
        lastAd = PlayerPrefs.GetString("Time");

        numberOfBananas = PlayerPrefs.GetInt("Bananas");
       
        if(String.Compare(DateTime.Now.ToString(),PlayerPrefs.GetString("Time"))>0)
        {
            PlayerPrefs.SetInt("Ad completed", 0);
            PlayerPrefs.Save();
        }

        adCompleted = PlayerPrefs.GetInt("Ad completed");
    }

    void OnLevelWasLoaded(int Level)
    {
        if(Level==0)
        {
            PlayerPrefs.SetInt("StartMenuLoaded", PlayerPrefs.GetInt("StartMenuLoaded", 0) + 1);
            PlayerPrefs.Save();

        }
        if (PlayerPrefs.GetInt("Ad completed") == 1)
        {

            PlayerPrefs.SetString("Time", DateTime.Now.AddHours(6).ToString());
            PlayerPrefs.Save();
        }
        else if (PlayerPrefs.GetInt("StartMenuLoaded")>1&&PlayerPrefs.GetInt("Ad completed") == 0)
        {
            Instantiate(getBananasPrefab, getBananasPrefabPosition.transform.position, Quaternion.identity);
        }

    }
  
  


    public void MusicSettings()
    {
       
        if(PlayerPrefs.GetString("music")=="Music: On")
        {
          
            PlayerPrefs.SetString("music", "Music: Off");
            PlayerPrefs.Save();
            musicText.text = PlayerPrefs.GetString("music");
            
        }
        else if (PlayerPrefs.GetString("music") == "Music: Off")
        {
          
            PlayerPrefs.SetString("music", "Music: On");
            PlayerPrefs.Save();
            musicText.text = PlayerPrefs.GetString("music");
        }
    }
  

}
